#Ilia Potanin
#5/25/18
#This script generates a daily controller status message based on the last two hours of controller logs


import datetime
import glob
from os import path

def proccess_Pressure_Log( data):
    datapointCount =  len(data)

    #if there are less than 1400 datapoints in the file only process the existing datapoints 
    if datapointCount < 1400 :
        stopCount = datapointCount
    else:
        stopCount = 1400
    #load in all the data points into a structure 
    cyc_readings = [[],[],[],[],[]]
    ori_readings = [[],[],[],[],[]]
    for line in data[1:stopCount]:
        readings = line.split(",")
        for x in range (0,5):
            cyc_readings[x].append(float(readings[5*x+6]))
            ori_readings[x].append(float(readings[5*x+7]))

    #calculate averages
    avg_CYC = []
    avg_ORI = []
    for collection in cyc_readings:
        meanValue = sum(collection)/ len(collection)

        #if the value is too large show 99 
        #if positive is a small negative number display a zero
        #if number is a large negartive number display -9 
        if meanValue >= 1:
            result = 999
        elif  meanValue < 0:
            if meanValue > -0.1 :
                result = 0
            else :
                result = -99
        else :
            result = round(1000 * meanValue)
        avg_CYC.append(int (result))

    for collection in ori_readings:
        meanValue = sum(collection)/ len(collection)
        #if the value is too large show 99
        #if positive is a small negative number display a zero
        #if number is a large negartive number display -9
        if meanValue >= 20:
            result = 999
        elif  meanValue < 0:
                result = -99
        else :
            result = round(10 * meanValue)
        avg_ORI.append(int(result))
    
    #Display and additional error if there are more then 5 minutes of missing data 
    error_MSG = ""
    today = datetime.datetime.today()
    correct_Count = today.hour *60 + today.minute
    difference = correct_Count - datapointCount
    if difference > 5 :
        error_MSG += ',G' + str(difference) 
          
    #Display results 
    return( '{0[0]:03d},{1[0]:03d},{0[1]:03d},{1[1]:03d},{0[2]:03d},{1[2]:03d},{0[3]:03d},{1[3]:03d},{0[4]:03d},{1[4]:03d}'.format(avg_ORI,avg_CYC) + error_MSG )


def make_Message():

    #get todays date
    today = datetime.datetime.today()
    currentDate =  today.date().isoformat()
    
    with open("/etc/samplername" , "r" ) as f:
             siteName = f.readline();
    #get combined file for today
    filePath = "/usr/local/output/*combined-" + currentDate + ".csv"

    #open file if it exists and load the data
    filesFound = glob.glob(filePath)
    if len(filesFound) > 0 :

        with open(filesFound[0] , "r" ) as f:
             data = f.readlines();
        return siteName + "," + proccess_Pressure_Log(data)    
    else:
        return "No File"

