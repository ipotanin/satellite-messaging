#Daniel Mascareno
# 5/14/18

# Creating a file to read in data from a file and parsing it using commas


with open("/usr/local/satellite-messaging/TEST-combined-2018.txt", "r") as f:
	data = f.readlines(); # Store file in array by line; data[0] is the first line of the file



print(data[34])

#parsed_data = []

for l in range (0,35):
	parsed_data = data[l].split(",")

print(parsed_data)


num_lines = len(data)  #variable to know how many lines there are in the file AKA measurements/module

avg_A1 = 0
avg_A2 = 0

avg_B1 = 0
avg_B2 = 0


avg_C1 = 0
avg_C2 = 0

avg_D1 = 0
avg_D2 = 0

avg_X1 = 0
avg_X2 = 0

for j in range (0,num_lines):
	arr = data[j].split(",");  #split each line when you see a comma 
	
	if float(arr[6]) > 0:   #Ilia doesn't want the -999.99 readings in the average
		avg_A1 = avg_A1 + float(arr[6])  #add all sensor readings together

	if float(arr[7]) > 0:   #Ilia doesn't want the -999.99 readings in the average
		avg_A2 = avg_A2 + float(arr[7])

	
	if float(arr[11]) > 0:
		avg_B1 = avg_B1 + float(arr[11])



	



real_avgA1 = avg_A1/num_lines   
real_avgA2 = avg_A2/num_lines   


real_avgB1 = avg_B1/num_lines   
real_avgB2 = avg_B2/num_lines   


real_avgC1 = avg_C1/num_lines   
#real_avgC2 = avg_C2/num_lines   

resultA1 = '{0:.3f}'.format(real_avgA1) #print the output with three numbers after decimal point
resultA2 = '{0:.2f}'.format(real_avgA2) #print the output with three numbers after decimal point


resultB1 = '{0:.3f}'.format(real_avgB1) #print the output with three numbers after decimal point
resultB2 = '{0:.2f}'.format(real_avgB2) #print the output with three numbers after decimal point


resultC1 = '{0:.3f}'.format(real_avgC1) #print the output with three numbers after decimal point
#resultC2 = '{0:.2f}'.format(real_avgC2) #print the output with three numbers after decimal point

#print (arr[0], ",",arr[2],  ",", resultA1, ",", resultA2, ",", resultB1, ",", resultB2, ",", resultC1,sep='') #sep='' to take off the spaces when you print mult arg


