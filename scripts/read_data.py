#Daniel Mascareno
#May 11, 2018


with open("/usr/local/satellite-messaging/TEST-combined-2018.txt", "r") as f:
	f.seek(56)
	#read sensor data one at a time
	data1 = f.read(8)
	
	#move cursor by 1
	f.seek((f.tell() + 1))
	data2 = f.read(9)

print ("The data from the sensors are data1: ", data1, "and data2:", data2)


# convert from string  to float and then add 
add = float(data1) + float(data2)


print ("Adding them is:", add)
