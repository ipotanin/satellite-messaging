#!/bin/bash

# save  the next line in crontab -e to schedule syncing everyday at 11:30 pm
#30 23 * * *  /usr/local/satellite-messaging/sendMessage.sh >> /usr/local/output/satelliteTexts.txt 2>&1

cd /usr/local/satellite-messaging/ 
echo "Transmition Started at $(date)"
python2 sendMessage.py
echo "Done Transmitting at $(date)"
