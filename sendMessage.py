import rockBlock
import sys, getopt
 
from rockBlock import rockBlockProtocol
import get_Controller_Status 
class SendMessage (rockBlockProtocol):
    
    def main( argv):
        
        try:
            opts, args = getopt.getopt(argv,"m:r",["message=","readings"])
        except getopt.GetoptError:
            print 'test.py -i <inputfile> -o <outputfile>'
            sys.exit(2)
        for opt, arg in opts:
            if opt in ("-m", "--message"):
                message = arg
            elif opt in ("-r", "--readings"):
                gen = get_Controller_Status 
                message = gen.make_Message()
                
        rb = rockBlock.rockBlock("/dev/ttyUSB0", self)
        rb.sendMessage(message)      
        rb.close()
        
    def rockBlockTxStarted(self):
        print("rockBlockTxStarted")
        
    def rockBlockTxFailed(self):
        print("rockBlockTxFailed")
        
    def rockBlockTxSuccess(self,momsn):
        print("rockBlockTxSuccess " + str(momsn))
        
if __name__ == '__main__': 
    SendMessage().main(sys.argv[1:])
