import rockBlock
import glob
import os
from rockBlock import rockBlockProtocol


class CheckSampleChange (rockBlockProtocol):

    def main(self):

        rb = rockBlock.rockBlock("/dev/ttyUSB0", self)
        os.chdir("/usr/local/output")
        if (os.path.exists('last_filterchange.txt')):
            with open('last_filterchange.txt','r') as reader:
                lastFile = reader.readline()
        else:
            lastFile = ""
        #find latest filer change
        filterReadings = glob.glob('*leakcheck*')
        filterReadings.sort()
        if (lastFile == filterReadings[-1]):
            rb.close()
            return
        with open('last_filterchange.txt', 'w') as writer:
            writer.write(filterReadings[-1])
        
        with open(filterReadings[-1],'r') as reader:
            msg = ""
            for lin in reader.readlines()[1:]:
                sec = lin.split(",")
                msg += sec[2][0:1]
                msg += sec[1][0:1]
                msg += sec[4][0:2]
                msg += sec[5][0:2] + '\n'
                init= sec[-1]
            msg = init+msg

        
        rb.sendMessage(msg)
        print(msg)
        rb.close()

    def rockBlockTxStarted(self):
        print("rockBlockTxStarted")

    def rockBlockTxFailed(self):
        print("rockBlockTxFailed")

    def rockBlockTxSuccess(self,momsn):
        print("rockBlockTxSuccess " + str(momsn))

if __name__ == '__main__':
    CheckSampleChange().main()

